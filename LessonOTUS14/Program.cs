﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LessonOTUS14
{
    public class Program
    {
        public static void Main(string[] args)
        {
            List<City> cities = new List<City>
            {
                new City() { Name = "Krasnodar" },
                new City() { Name = "Moscow" },
                new City() { Name = "Krasnoyarsk" },
                new City() { Name = "Ykb" },
                new City() { Name = "Bratsk" },
                new City() { Name = "Irkutsk" },
                new City() { Name = "Novosibirsk" },
                new City() { Name = "Shelehov" },
                new City() { Name = "Angarsk" }
            };

            Graf<Vertex> graf = new Graf<Vertex>();
            graf.Add(new Vertex() { FirstCity = cities[0], SecondCity = cities[1], Distance = 5 });
            graf.Add(new Vertex() { FirstCity = cities[0], SecondCity = cities[1], Distance = 5 });
            graf.Add(new Vertex() { FirstCity = cities[0], SecondCity = cities[1], Distance = 5 });
            graf.Add(new Vertex() { FirstCity = cities[0], SecondCity = cities[1], Distance = 5 });
            var test = graf.GetDataById(1); //?
        }
    }
}

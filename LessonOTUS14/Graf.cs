﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LessonOTUS14
{
    public class Graf<T> : INode<T>
    {
        int Id;
        List<T> Data = default;

        public void Add(T data)
        {
            Data.Add(data);
        }

        public bool Contains(T data)
        {
            throw new NotImplementedException();
        }

        public T GetDataById(int id)
        {
            throw new NotImplementedException();
        }

        public void Remove(int id)
        {
            throw new NotImplementedException();
        }

        //public void Add(City f, City s, int l)
        //{
        //    Cities.Add(new Vertex() { FirstCity = f, SecondCity = s, Distance = 100 });
        //}

        //public bool Contains(City city)
        //{
        //    return Cities.Contains(city);
        //}

        //public void GetDataById()
        //{
        //    throw new NotImplementedException();
        //}

        //public void Remove(City city)
        //{
        //    Cities.Remove(city);
        //}
    }

    /// <summary>
    /// Vertex
    /// </summary>
    public struct Vertex
    {
        public City FirstCity, SecondCity;
        public int Distance;
        public Vertex(City f, City s, int l)
        {
            FirstCity = f;
            SecondCity = s;
            Distance = l;
        }
    }

    public struct City
    {
        public string Name;
        public City(string name)
        {
            Name = name;
        }
    }
}

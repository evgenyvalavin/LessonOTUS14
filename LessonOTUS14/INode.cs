﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LessonOTUS14
{
    public interface INode<T>
    {
        void Add(T data);
        bool Contains(T data);
        void Remove(int id);
        T GetDataById(int id);
    }
}
